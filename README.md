# <img src="https://gitlab.com/uploads/-/system/user/avatar/5273412/avatar.png?width=100" alt="drawing" width="70"/> Edgar Omar Valles Castro ✌🏽

## Acerca de mí 👨🏽‍💻
Soy un joven ingeniero en sistemas computacionales, egresado del Instituto Tecnologico de Durango con especialidad en el desarrollo de aplicaciones móviles.

## Un poco de mí 🌮🇲🇽
Egresado en el año 2020, donde inicie como desarrollador móvil con java, despues las tendencias me llevaron al desarrollo con <a src=https://learn.microsoft.com/es-es/xamarin/get-started/what-is-xamarin>xamarin</a>, desde ese momento descubri mi pasión por las tecnologías móviles emergentes.

## Tecnologías Móviles 📱
[![Xamarin](https://img.shields.io/badge/xamarin-023047?style=for-the-badge&logo=xamarin&logoColor=white&labelColor=101010)]()
[![Swift](https://img.shields.io/badge/Swift-fb8500?style=for-the-badge&logo=Swift&logoColor=white&labelColor=101010)]()
[![Ionic](https://img.shields.io/badge/Ionic-caf0f8?style=for-the-badge&logo=ionic&logoColor=white&labelColor=101010)]()
[![Android](https://img.shields.io/badge/Android-344e41?style=for-the-badge&logo=Android&logoColor=white&labelColor=101010)]()

## Tecnlogías Web 🌎
[![Angular](https://img.shields.io/badge/Angular-e63946?style=for-the-badge&logo=Angular&logoColor=white&labelColor=101010)]()
[![Node](https://img.shields.io/badge/Node-0d1b2a?style=for-the-badge&logo=npm&logoColor=white&labelColor=101010)]()
[![.NET](https://img.shields.io/badge/.NET-2b2d42?style=for-the-badge&logo=.net&logoColor=white&labelColor=101010)]()
[![.NET Core](https://img.shields.io/badge/.NET_Core-2b2d42?style=for-the-badge&logo=.net&logoColor=white&labelColor=101010)]()
[![Laravel](https://img.shields.io/badge/Laravel-9b2226?style=for-the-badge&logo=larevel&logoColor=white&labelColor=101010)]()

## Tecnologia SQL 💾
[![SQL Server](https://img.shields.io/badge/SQL_Server-e63946?style=for-the-badge&logo=microsoft&logoColor=white&labelColor=101010)]()
[![MySql](https://img.shields.io/badge/MySQL-184e77?style=for-the-badge&logo=mysql&logoColor=white&labelColor=101010)]()

## Tecnologías de escritorio 🖥️
[![C#](https://img.shields.io/badge/C_Sharp-669bbc?style=for-the-badge&logo=csharp&logoColor=white&labelColor=101010)]()
[![Java](https://img.shields.io/badge/java-9b2226?style=for-the-badge&logo=java&logoColor=white&labelColor=101010)]()

## Experiencia 📑
```
En el desarrollo móvil con Xamarin cuento con tres años de
experiencia, en los cuales he enfrentado retos como la
integración nativa de librerías, creación de custom
renders, custom control, publicación en las tiendas de 
App Store y Play Store
```
```
En el desarrollo de back end tengo experiencia en diferentes tecnologías de desarrollo como
 - Angular
 - Laravel
 - Node JS
 - .Net y .Net Core
```
## Contacto:

[![Email](https://img.shields.io/badge/valles.castro96@gmail.com-email_personal-D14836?style=for-the-badge&logo=gmail&logoColor=white&labelColor=101010)](mailto:valles.castro96@gmail.com)
</br>
[![Linkedin](https://img.shields.io/badge/Linkedin-FFDD00?style=for-the-badge&linkedin&logoColor=white&labelColor=101010)](https://www.linkedin.com/in/edgar-omar-valles-castro-aa5a32198/)